package com.project.lttbdd.p2p.p2p_share_files;

import android.content.Context;
import android.content.IntentFilter;
import android.net.wifi.WpsInfo;
import android.net.wifi.p2p.WifiP2pConfig;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pDeviceList;
import android.net.wifi.p2p.WifiP2pGroup;
import android.net.wifi.p2p.WifiP2pInfo;
import android.net.wifi.p2p.WifiP2pManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.project.lttbdd.p2p.R;

import java.util.ArrayList;
import java.util.List;

public class P2pActivity extends AppCompatActivity {

    private WifiP2pDevice m_thisDevice = new WifiP2pDevice();
    private WifiP2pManager m_p2pManager;
    private final IntentFilter m_intentFilter = new IntentFilter();
    private WifiP2pManager.Channel m_channel;
    private WiFiDirectBroadcastReceiver m_receiver;
    protected WifiP2pManager.PeerListListener m_peerListListener;
    protected WifiP2pManager.ActionListener m_p2pActionListener;
    protected WifiP2pManager.GroupInfoListener m_p2pGroupListener;
    protected WifiP2pManager.ConnectionInfoListener m_p2pConnectionListener;
    private List<WifiP2pDevice> m_peers = new ArrayList<WifiP2pDevice>();
    private Boolean m_p2pEnable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_p2p);

        getInfoDevice();
        setUpP2P();
        discoveryPeers();
    }

    private void getInfoDevice(){
        m_thisDevice.primaryDeviceType = getApplicationContext().getResources().getString(
                com.android.internal.R.string.config_wifi_p2p_device_type);
        m_thisDevice.deviceName = "Android_" + Settings.Secure.getString(getApplicationContext().getContentResolver(),
                Settings.Secure.ANDROID_ID);
    }

    private final void setUpP2P(){
        //  Indicates a change in the Wi-Fi P2P status.
        m_intentFilter.addAction(WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION);
        // Indicates a change in the list of available peers.
        m_intentFilter.addAction(WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION);
        // Indicates the state of Wi-Fi P2P connectivity has changed.
        m_intentFilter.addAction(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION);
        // Indicates this device's details have changed.
        m_intentFilter.addAction(WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION);

        m_p2pManager = (WifiP2pManager) getSystemService(Context.WIFI_P2P_SERVICE);
        m_channel = m_p2pManager.initialize(this, getMainLooper(), null);

        //treat infos received for each event
        //receive information about discovered peers
        m_peerListListener = new WifiP2pManager.PeerListListener() {
            @Override
            public void onPeersAvailable(WifiP2pDeviceList peerList) {

                Log.i("Peer list listener", "");
                List<WifiP2pDevice> refreshedPeers = (List<WifiP2pDevice>) peerList.getDeviceList();
                if (!refreshedPeers.equals(m_peers)) {
                    //update list peers
                    m_peers.clear();
                    m_peers.addAll(refreshedPeers);

                    for(Integer i = 0; i < m_peers.size(); i++) {
                        Log.i("Peer " + i.toString() + ": ", m_peers.get(i).toString());
                    }
                }

                if (m_peers.size() == 0) {
                    Log.i("", "No devices found");
                    return;
                }
                else{
                    connectTo(0);
                }
            }
        };

        //receive information about a P2P group
        m_p2pGroupListener = new WifiP2pManager.GroupInfoListener() {
            @Override
            public void onGroupInfoAvailable(WifiP2pGroup group) {
                List<WifiP2pDevice> peers = (List<WifiP2pDevice>) group.getClientList();
                for(Integer i = 0; i < m_peers.size(); i++) {
                    Log.i("group member " + i.toString() + ": ", peers.get(i).toString());
                }
                String groupPassword = group.getPassphrase();
                Log.i("Pass group: ", groupPassword);
            }
        };

        //receive information about the current connection
        m_p2pConnectionListener = new WifiP2pManager.ConnectionInfoListener(){
            @Override
            public void onConnectionInfoAvailable(final WifiP2pInfo info) {

                // InetAddress from WifiP2pInfo struct.
                String groupOwnerAddress = info.groupOwnerAddress.getHostAddress();

                // After the group negotiation, we can determine the group owner
                // (server).
                if (info.groupFormed && info.isGroupOwner) {
                    // Do whatever tasks are specific to the group owner.
                    // One common case is creating a group owner thread and accepting
                    // incoming connections.
                } else if (info.groupFormed) {
                    // The other device acts as the peer (client). In this case,
                    // you'll want to create a peer thread that connects
                    // to the group owner.
                }
            }
        };
    }

    private void discoveryPeers(){
        m_p2pManager.discoverPeers(m_channel, new WifiP2pManager.ActionListener(){
            @Override
            public void onSuccess() {
                // WiFiDirectBroadcastReceiver will notify us. Ignore for now.
            }

            @Override
            public void onFailure(int reason) {
                Toast.makeText(P2pActivity.this, "Discovery peers failed.",
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void stopDiscoveryPeers(){
        m_p2pManager.stopPeerDiscovery(m_channel, new WifiP2pManager.ActionListener(){
            @Override
            public void onSuccess() {
                // WiFiDirectBroadcastReceiver will notify us. Ignore for now.
            }

            @Override
            public void onFailure(int reason) {
                Toast.makeText(P2pActivity.this, "Stop discovery peers failed.",
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void setIsWifiP2pEnabled(boolean enable){
        m_p2pEnable = enable;
        Log.i("wifi p2p enable", m_p2pEnable.toString());
    }

    public void connectTo(int i) {
        // Picking the first device found on the network.
        final WifiP2pDevice device = m_peers.get(i);

        final WifiP2pConfig config = new WifiP2pConfig();
        config.deviceAddress = device.deviceAddress;
        config.wps.setup = WpsInfo.PBC;

        m_p2pManager.connect(m_channel, config, new WifiP2pManager.ActionListener() {

            @Override
            public void onSuccess() {
                // WiFiDirectBroadcastReceiver will notify us. Ignore for now.
                Log.i("connect to " + device.deviceName.toString(), " Success");
            }

            @Override
            public void onFailure(int reason) {
                Toast.makeText(P2pActivity.this, "Connect failed. Retry.",
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        m_receiver = new WiFiDirectBroadcastReceiver(m_p2pManager, m_channel, this);
        registerReceiver(m_receiver, m_intentFilter);
    }

    @Override
    public void onPause() {
        super.onPause();
        unregisterReceiver(m_receiver);
    }
}
