package com.project.lttbdd.p2p.p2p_share_files;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.NetworkInfo;
import android.net.wifi.p2p.WifiP2pManager;
import android.util.Log;

/**
 * Created by VUDAI on 4/9/2017.
 */

public class WiFiDirectBroadcastReceiver extends BroadcastReceiver {

    private final P2pActivity m_p2pActivity;
    private final WifiP2pManager m_p2pManager;
    private final WifiP2pManager.Channel m_channel;

    public WiFiDirectBroadcastReceiver(WifiP2pManager p2pManager, WifiP2pManager.Channel channel, P2pActivity p2pActivity) {
        m_p2pActivity = p2pActivity;
        m_p2pManager = p2pManager;
        m_channel = channel;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i("receiver: ", "on receive");
        String action = intent.getAction();
        if (WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION.equals(action)) {
            Log.i("receiver: ", "state changed");
            // Determine if Wifi P2P mode is enabled or not, alert
            // the Activity.
            int state = intent.getIntExtra(WifiP2pManager.EXTRA_WIFI_STATE, -1);
            if (state == WifiP2pManager.WIFI_P2P_STATE_ENABLED) {
                m_p2pActivity.setIsWifiP2pEnabled(true);
            } else {
                m_p2pActivity.setIsWifiP2pEnabled(false);
            }
        } else if (WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION.equals(action)) {

            Log.i("receiver: ", "peers changed");
            // The peer list has changed!  We should probably do something about
            // that.
            if (m_p2pManager != null) {
                m_p2pManager.requestPeers(m_channel, m_p2pActivity.m_peerListListener);
            }
            Log.d("", "P2P peers changed");

        } else if (WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION.equals(action)) {

            Log.i("receiver: ", "connection changed");
            // Connection state changed!  We should probably do something about
            // that.
            if (m_p2pManager == null) {
                return;
            }

            NetworkInfo networkInfo = (NetworkInfo) intent
                    .getParcelableExtra(WifiP2pManager.EXTRA_NETWORK_INFO);

            if (networkInfo.isConnected()) {
                // We are connected with the other device, request connection
                // info to find group owner IP
                m_p2pManager.requestConnectionInfo(m_channel, m_p2pActivity.m_p2pConnectionListener);
            }

        } else if (WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION.equals(action)) {

            Log.i("receiver: ", "devide changed");
        }
    }
}
