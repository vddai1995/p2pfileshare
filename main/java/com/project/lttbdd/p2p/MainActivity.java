package com.project.lttbdd.p2p;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.project.lttbdd.p2p.p2p_share_files.P2pActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void startP2pActivity(View v){
        Log.i("MainActivity ", "start P2pActivity");
        Intent intent = new Intent(getApplicationContext(), P2pActivity.class);
        startActivity(intent);
    }
}
